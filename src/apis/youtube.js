import axios from 'axios';

const KEY = 'AIzaSyC5OMdMzHxUULaTPxfa9bb8emyec5-KRP8';

export default axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3',
  params: {
    part: 'snippet',
    maxResults: 5,
    key: KEY,
    type: 'video',
  },
});
