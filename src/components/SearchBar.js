import React, { useState } from 'react';
import PropTypes from 'prop-types';

const SearchBar = ({ onTermSubmit }) => {
  const [term, setTerm] = useState('');

  const onInputChange = e => setTerm(e.target.value);

  const onFormSubmit = e => {
    e.preventDefault();
    onTermSubmit(term);
  };

  return (
    <div className='search-bar ui segment'>
      <form className='ui form' onSubmit={onFormSubmit}>
        <div className='field'>
          <label>Video Search</label>
          <input type='text' value={term} onChange={onInputChange} />
        </div>
      </form>
    </div>
  );
};

SearchBar.propTypes = {
  onTermSubmit: PropTypes.func.isRequired,
};

export default SearchBar;
