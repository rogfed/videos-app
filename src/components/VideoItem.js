import React from 'react';
import PropTypes from 'prop-types';

import '../styles/VideoItem.scss';

const VideoItem = ({ video, onVideoSelect }) => {
  const { title, thumbnails } = video.snippet;

  const onVideoClick = () => {
    onVideoSelect(video);
  };

  return (
    <div className='item video-item' onClick={onVideoClick}>
      <img className='ui image' src={thumbnails.medium.url} alt={title} />
      <div className='content'>
        <div className='header'>{title}</div>
      </div>
    </div>
  );
};

VideoItem.propTypes = {
  video: PropTypes.object.isRequired,
  onVideoSelect: PropTypes.func.isRequired,
};

export default VideoItem;
